[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)

# Mapping

[[_TOC_]]

## About

This package contains information for integrating tracking and mapping into the manipulation pipeline for the purposes of integrating a moving base and maintaining a consistent world view for object detection and collision avoidance.

## Tracking

## World Mapping

Currently we support nvblox (the ROS1 port from ETZH lab) for TSDF-based, GPU accelerated mapping. The mapping is done inside a docker image hosted [here](registry.gitlab.com/apl-ocean-engineering/raven/manipulation/mapping/mapping:base) or you can build your own (see the [Dockerfile](docker/Dockerfile)).

Run the docker from inside the `docker/` folder: `docker compose up` and then enter the container: `docker exec -it mapping /bin/bash`.
`mapping` gets bind-mounted inside so that you can edit the configurations and launch files from the host computer.

Results look like this:

![tank_map](assets/tank_map.png)


## Map Server

The map_server manages the map environment and integrates with the MoveIt PlanningScene interface.
It also provides a framework for integrating actions. Currently, it supports the UpdateCollisionMatrix action which turns on/off collision for links in the robot.
