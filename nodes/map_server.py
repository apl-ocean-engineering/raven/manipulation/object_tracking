#! /usr/bin/env python3

"""
Collision Mapping Node

Author: Marc Micatka
University of Washington - Applied Physics Lab, 2025
"""

import sys

import moveit_commander
import rospy
import tf2_ros
from geometry_msgs.msg import Point, Pose, Quaternion
from moveit_msgs.msg import CollisionObject, PlanningScene
from shape_msgs.msg import Mesh
from std_srvs.srv import SetBool, SetBoolResponse

import nvblox_msgs.msg
from mapping.mapping_utilities import nvblox_mesh_to_shape_mesh


class MapServer:
    """
    Class to manage the world map via a Moveit PlanningScene
    """

    def __init__(self):
        moveit_commander.roscpp_initialize(sys.argv)
        self.scene = moveit_commander.PlanningSceneInterface(synchronous=True)

        # For transform stuff
        self._buffer = tf2_ros.Buffer()
        self._listener = tf2_ros.TransformListener(self._buffer)

        # Parameters
        self._verbose = rospy.get_param("~verbose", False)
        self._planning_frame = rospy.get_param("~planning_frame", "map")

        # Run Parameters
        self.rate = rospy.Rate(20)

        # Detection stuff
        self._current_object_mesh = None
        self._new_object_map = False
        self._current_background_mesh = None
        self._new_background_map = False
        self.pause_updates = False
        # Setup Publications
        self.scene_pub = rospy.Publisher(
            "~planning_scene", PlanningScene, queue_size=10
        )

        # Setup Subscriptions
        self.object_map_sub = rospy.Subscriber(
            "~object_map", nvblox_msgs.msg.Mesh, self.object_map_callback
        )
        self.background_map_sub = rospy.Subscriber(
            "~background_map", nvblox_msgs.msg.Mesh, self.background_map_callback
        )

        rospy.Service('/pause_scene_updates', SetBool, self.pause_updates_handler)

    def pause_updates_handler(self, req):
        rospy.loginfo("Scene updates paused" if req.data else "Scene updates resumed")
        self.pause_updates = req.data
        return SetBoolResponse(success=True, message="Scene updates paused" if req.data else "Scene updates resumed")


    def object_map_callback(self, msg: Mesh):
        if self._planning_frame != msg.header.frame_id:
            rospy.logwarn(
                f"object nvblox msg has frame: {msg.header.frame_id} but we want planning frame: {self._planning_frame}"
            )

        mesh_from_msg = nvblox_mesh_to_shape_mesh(msg)
        self._current_object_map_mesh = mesh_from_msg
        self._new_object_map = True

    def background_map_callback(self, msg: Mesh):
        if self._planning_frame != msg.header.frame_id:
            rospy.logwarn(
                f"background nvblox msg has frame: {msg.header.frame_id} but we want planning frame: {self._planning_frame}"
            )
        mesh_from_msg = nvblox_mesh_to_shape_mesh(msg)

        self._current_background_map_mesh = mesh_from_msg
        self._new_background_map = True

    def add_mesh_to_planning_scene(self, mesh, object_id: str):
        """
        Add a shape_msgs/Mesh to the MoveIt! planning scene as a collision object.
        """
        # Create a CollisionObject
        collision_object = CollisionObject()
        collision_object.header.frame_id = self._planning_frame
        collision_object.id = object_id
        collision_object.meshes.append(mesh)
        collision_object.mesh_poses.append(
            Pose(position=Point(0, 0, 0), orientation=Quaternion(0, 0, 0, 1))
        )

        # Add the object to the planning scene
        planning_scene = PlanningScene()
        planning_scene.world.collision_objects.append(collision_object)
        planning_scene.is_diff = True

        try:
            response = self.scene.apply_planning_scene(planning_scene)
            if self._verbose:
                if not response:
                    rospy.logerr("Failed to apply planning scene")
                else:
                    rospy.loginfo_throttle(5, "Successfully applied planning scene!")
        except rospy.ServiceException as e:
            rospy.logerr(f"Service call failed: {e}")

    def run(self):
        while not rospy.is_shutdown():
            if not self.pause_updates:
                if self._new_background_map:
                    self.add_mesh_to_planning_scene(
                        self._current_background_map_mesh, "background_mesh"
                    )
                    self._new_background_map = False
                if self._new_object_map:
                    self.add_mesh_to_planning_scene(
                        self._current_object_map_mesh, "object_mesh"
                    )
                    self._new_object_map = False
            self.rate.sleep()


if __name__ == "__main__":
    try:
        rospy.init_node("map_server")
        rospy.loginfo("Starting map server.")
        map_server = MapServer()
        map_server.run()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
