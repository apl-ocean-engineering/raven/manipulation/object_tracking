"""
downsampling node

Author: Marc Micatka
"""

import cv2
import message_filters
import numpy as np
import rospy
from sensor_msgs.msg import CameraInfo, Image


# Function to downsample image
def downsample_image(image_msg, downsample_factor=2):
    # Convert image message to numpy array
    height = image_msg.height
    width = image_msg.width
    img_np = np.frombuffer(image_msg.data, dtype=np.uint8).reshape((height, width))

    # Convert Bayer RGGB8
    img = cv2.cvtColor(img_np, cv2.COLOR_BAYER_BG2RGB)
    # Downsample the image
    downsampled = cv2.resize(
        img, (width // downsample_factor, height // downsample_factor)
    )

    # Convert BGR to grayscale (Mono8)

    # Create a new Image message
    downsampled_image_msg = Image()
    downsampled_image_msg.header = image_msg.header
    downsampled_image_msg.height = height // downsample_factor
    downsampled_image_msg.width = width // downsample_factor
    downsampled_image_msg.encoding = "rgb8"
    downsampled_image_msg.step = downsampled.shape[1] * 3
    downsampled_image_msg.data = downsampled.tobytes()

    return downsampled_image_msg


# Function to downsample camera info
def downsample_camera_info(camera_info_msg, downsample_factor=2):
    # Downsample the camera info parameters accordingly
    camera_info_msg.height //= downsample_factor
    camera_info_msg.width //= downsample_factor
    new_k = list(camera_info_msg.K)

    new_k[0] /= downsample_factor
    new_k[2] /= downsample_factor
    new_k[4] /= downsample_factor
    new_k[5] /= downsample_factor
    camera_info_msg.K = new_k

    new_p = list(camera_info_msg.P)
    new_p[0] /= downsample_factor  # Adjust focal length along x-axis
    new_p[2] /= downsample_factor  # Adjust optical center along x-axis
    new_p[3] /= downsample_factor  # Adjust optical center along x-axis
    new_p[5] /= downsample_factor  # Adjust focal length along y-axis
    new_p[6] /= downsample_factor  # Adjust optical center along y-axis
    new_p[7] /= downsample_factor  # Adjust optical center along y-axis

    camera_info_msg.P = new_p

    return camera_info_msg


class Downsampler:
    def __init__(self) -> None:

        self.downsample_factor = rospy.get_param("~downsample_factor", 2)
        self.left_image_pub = rospy.Publisher(
            "~left/image_downsampled", Image, queue_size=5
        )
        self.left_info_pub = rospy.Publisher(
            "~left/image_downsampled/camera_info", CameraInfo, queue_size=5
        )
        self.right_image_pub = rospy.Publisher(
            "~right/image_downsampled", Image, queue_size=5
        )
        self.right_info_pub = rospy.Publisher(
            "~right/image_downsampled/camera_info", CameraInfo, queue_size=5
        )
        self.left_sub = message_filters.Subscriber(
            "/raven/stereo/left/image_raw", Image
        )
        self.left_info_sub = message_filters.Subscriber(
            "/raven/stereo/left/camera_info", CameraInfo
        )

        self.right_sub = message_filters.Subscriber(
            "/raven/stereo/right/image_raw", Image
        )
        self.right_info_sub = message_filters.Subscriber(
            "/raven/stereo/right/camera_info", CameraInfo
        )

        approx_sync = message_filters.ApproximateTimeSynchronizer(
            [
                self.left_sub,
                self.left_info_sub,
                self.right_sub,
                self.right_info_sub,
            ],
            queue_size=5,
            slop=0.1,
        )
        approx_sync.registerCallback(self.synchronized_callback)

    def synchronized_callback(
        self,
        left_camera_msg: Image,
        left_info_msg: CameraInfo,
        right_camera_msg: Image,
        right_info_msg: CameraInfo,
    ):

        left_msg = downsample_image(left_camera_msg, self.downsample_factor)
        left_info_msg = downsample_camera_info(left_info_msg, self.downsample_factor)
        right_msg = downsample_image(right_camera_msg, self.downsample_factor)
        right_info_msg = downsample_camera_info(right_info_msg, self.downsample_factor)

        # Force same timestamps:
        left_msg.header.stamp = left_camera_msg.header.stamp
        left_info_msg.header.stamp = left_camera_msg.header.stamp

        right_msg.header.stamp = left_camera_msg.header.stamp
        right_info_msg.header.stamp = left_camera_msg.header.stamp

        self.left_image_pub.publish(left_msg)
        self.left_info_pub.publish(left_info_msg)

        self.right_image_pub.publish(right_msg)
        self.right_info_pub.publish(right_info_msg)


if __name__ == "__main__":
    try:
        rospy.init_node("downsampler")
        downsampler = Downsampler()
        rospy.loginfo("Starting downsampler.")
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
