from shape_msgs.msg import Mesh, MeshTriangle
from geometry_msgs.msg import Point
import nvblox_msgs.msg


def nvblox_mesh_to_shape_mesh(nvblox_mesh_msg: nvblox_msgs.msg.Mesh) -> Mesh:
    """
    Convert an nvblox_msgs/Mesh to a shape_msgs/Mesh.

    Args:
        nvblox_mesh_msg (nvblox_msgs.msg.Mesh): input nvblox Mesh

    Returns:
        Mesh: output shape_msg/Mesh
    """
    moveit_mesh = Mesh()

    for block in nvblox_mesh_msg.blocks:
        # Convert vertices
        vertex_map = {}  # To map original indices to new ones in moveit mesh
        for i, vertex in enumerate(block.vertices):
            point = Point(x=vertex.x, y=vertex.y, z=vertex.z)
            vertex_map[i] = len(moveit_mesh.vertices)  # Store new index
            moveit_mesh.vertices.append(point)

        # Convert triangles (triangle indices refer to block.vertices)
        for i in range(0, len(block.triangles), 3):
            triangle = MeshTriangle()
            triangle.vertex_indices = [
                vertex_map[block.triangles[i]],
                vertex_map[block.triangles[i + 1]],
                vertex_map[block.triangles[i + 2]],
            ]
            moveit_mesh.triangles.append(triangle)

    return moveit_mesh
